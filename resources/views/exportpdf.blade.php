<!DOCTYPE html>
<html>
<head>
<style>
#customers {
  font-family: Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #8da2bc;
  color: white;
}
</style>
</head>
<body>


<table id="customers">
    <thead>
        <tr>
            <th style="width: 20px">ID</th>
            <th>Nama Tamu</th>
            <th>Kehadiran</th>
            
          </tr>
    </thead>
    <tbody>
      @foreach ($data as $item)
      <tr>
        <td style="text-align: center">{{$item->id_tamu}}</td>
        <td>{{$item->nama_tamu}}</td>
        <td>
          @if ($item->kehadiran==1)
              Hadir
          @else
              Tidak Hadir
          @endif
        </td>
    </tr>
      @endforeach
        
    </tbody>

</table>

</body>
</html>