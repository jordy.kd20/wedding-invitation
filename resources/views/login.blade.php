@extends('template')
<head>
    <title>
        Login
    </title>
</head>
@section('Content')
<body style="color: var(--bs-light);background: var(--bs-cyan);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 17px;">
                <div class="card shadow-lg o-hidden border-0 my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-flex">
                                <div class="flex-grow-1 bg-login-image" style="background: url(&quot;dogs/logo .jpg&quot;) center / contain no-repeat;"></div>
                            </div>
                            <div class="col-lg-6" style="background: var(--bs-cyan);">
                                <div class="d-block p-5" style="margin-top: 75px;margin-bottom: 75px;">
                                    <div class="text-center">
                                        <h4 class="text-dark mb-4">Login</h4>
                                    </div>
                                    <form method="POST" action="/dashboard/login">
                                        @csrf
                                        <div class="mb-3"><input class="form-control form-control-user" type="text" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Username" name="username"></div>
                                        <div class="mb-3"><input class="form-control form-control-user" type="password" id="exampleInputPassword" placeholder="Password" name="password"></div>
                                        <div class="mb-3"> <button class="btn btn-primary d-block btn-user w-100" style="margin-top: 0;font-size: 16px;height: 46px;" type="submit">Login</button>
                                            <div class="custom-control custom-checkbox small"></div>
                                        </div>
                                    </form>
                                    <div class="mb-3">
                                        <div class="custom-control custom-checkbox small"></div>
                                    </div>
                                    <div class="text-center"></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/bs-init.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="{{url('assets/js/stylish-portfolio.js')}}"></script>
</body>
@endsection