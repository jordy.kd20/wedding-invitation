@extends('admintemplate')
<head>
    <title>
        Dashboard Invitation
    </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.common.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>
@section('Content')
    <div id="wrapper">
        <nav class="navbar navbar-dark align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background: #171717;">
            <div class="container-fluid d-flex flex-column p-0"><a class="navbar-brand d-flex justify-content-center align-items-center sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><img src="{{url('assets\img\WhatsApp Image 2022-09-08 at 21.54.53.jpg')}}" style="width: 40px;height: 40px;transform: rotate(15deg);"></div>
                    <div class="sidebar-brand-text mx-3"><span style="font-size: 9px;">Memoire Invitation</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item"><a class="nav-link" href="/admin"><i class="fa fa-list-ul"></i><span>List Tamu</span></a></li>
                    {{-- <li class="nav-item"><a class="nav-link" href="/admin/listasset"><i class="fa fa-building"></i><span>List Asset</span></a>
                        <a class="nav-link" href="/admin/pembelianproduk"><i class="fa fa-cart-plus"></i><span>Pembelian produk</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-dollar"></i><span>List Pembelian Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-info"></i><span>List Retur Distributor</span></a></li>
                    <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-money"></i><span>Hutang Distributor</span></a></li> --}}
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle me-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button>
                        <ul class="navbar-nav flex-nowrap ms-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-end p-3 animated--grow-in" aria-labelledby="searchDropdown">
                                    <form class="me-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" aria-expanded="false" data-bs-toggle="dropdown" href="#"><span class="d-none d-lg-inline me-2 text-gray-600 small">ADMIN</span><img class="border rounded-circle img-profile" src='{{url('/asset/img/avatars/avatar5.jpeg')}}'></a>
                                    <div class="dropdown-menu shadow dropdown-menu-end animated--grow-in"><a class="dropdown-item" href="#"><i class="fas fa-user fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Edit Profile</a><a class="dropdown-item" href="/logout"><i class="fas fa-sign-out-alt fa-sm fa-fw me-2 text-gray-400"></i>&nbsp;Logout</a></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div class="container-fluid">
                    <div class="d-sm-flex justify-content-between align-items-center mb-4">
                        <h3 class="text-dark mb-0">List Tamu</h3>
                        <a class="btn btn-primary d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex flex-row-reverse justify-content-center align-items-center flex-sm-row-reverse flex-md-row-reverse flex-lg-row-reverse flex-xl-row-reverse justify-content-xl-center align-items-xl-center flex-xxl-row-reverse" style="margin-right: 10px;margin-bottom: 10px;" href="/exportpdf">PDF List Tamu</a> 
                    </div>
                    <div class="row">                       
                        <div class="card shadow mb-4">
                            <div class="card-header d-flex justify-content-between align-items-center">
                                <button class="btn btn-primary d-flex d-sm-flex d-md-flex d-lg-flex d-xl-flex d-xxl-flex flex-row-reverse justify-content-center align-items-center flex-sm-row-reverse flex-md-row-reverse flex-lg-row-reverse flex-xl-row-reverse justify-content-xl-center align-items-xl-center flex-xxl-row-reverse" type="button" data-bs-target="#modal-insert-barang" data-bs-toggle="modal" style="margin-right: 10px;margin-bottom: 10px;">Tambah Tamu<i class="fa fa-plus" style="margin-left: 0px;margin-right: 10px;"></i></button>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <table class="table table-stripped center" id="myTable" style="text-align: center"> 
                                        <thead>
                                            <tr>
                                                <th style="padding-right: 40px;padding-left: 40px;">Nama Tamu</th>
                                                <th>Jumlah Hadir</th>
                                                {{-- <th>ID Tamu</th> --}}
                                                <th>Link</th>
                                                <th>Send WA</th>
                                                <th>Kehadiran</th>
                                                <th>Photobooth</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($isi == 1)
                                                @foreach ($datatamu as $item)
                                                    <tr>
                                                        <td>{{$item->nama_tamu}}</td>
                                                        <td style="text-align: center">{{$item->jumlah_tamu}}</td>
                                                        {{-- <td>{{$item->id_tamu}}</td> --}}
                                                        <td><a href="/memoire/undangan/{{$item->id_tamu}}">bymemoire.xyz/memoire/undangan/{{$item->id_tamu}}</a></td>
                                                        
                                                        <td><a style="text-align: center" 
                                                            href="https://api.whatsapp.com/send?phone=&text=Selamat+Pagi%2FSiang%2FMalam+Bapak%2FIbu%2FSdr%2Fi.%20{{$item->nama_tamu}}%20Izinkan+kami+berbagi+momen+spesial+dalam+hidup+kami.+%0D%0A%0D%0AKlik+tautan+berikut+ini+yang+akan+membawa+Bapak%2FIbu%2FSdr%2Fi+menuju+Undangan+Resepsi+kami+%3A+%0D%0A%0D%0Ahttps%3A%2F%2Fbymemoire.xyz%2Fmemoire%2Fundangan%2F{{$item->id_tamu}}%0D%0A%0D%0AMerupakan+suatu+kebahagiaan+bagi+kami+jika+Bapak%2FIbu%2FSdr%2Fi+berkenan+menghadiri+momen+spesial+kami.%0A%0A%0AKami+yang+berbahagia%2C%0D%0AArdy%26Cynthia" class="btn btn-success" target="_blank"><i class="fa fa-whatsapp"></i></a></td>
                                                        <td>
                                                            @if ($item->kehadiran==0)
                                                                Tidak Hadir
                                                            @else
                                                                Hadir
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($item->photobooth==0)
                                                                Belom Foto 
                                                            @else
                                                                Sudah Foto
                                                            @endif
                                                            
                                                        </td>
                                    
                                                        <td><button href="" class="btn btn-warning" data-nama_tamu='{{$item->nama_tamu}}'' data-id_tamu="{{$item->id_tamu}}" data-jumlah_tamu="{{$item->jumlah_tamu}}" data-bs-toggle="modal" data-bs-target="#edit"><i class="fa fa-edit"></i></button>
                                                        &nbsp; <a href="/dashboardadmin/delete/{{$item->id_tamu}}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                        &nbsp; <a href="/absentamu/{{$item->id_tamu}}" class="btn btn-succsess"><i class="fa fa-check"></i></a></td>
                                                </tr>
                                                @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" role="dialog" tabindex="-1" id="modal-insert-barang">
                <form action="/dashboard/tambahorang" method="POST">
                    @csrf
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Tambah Tamu</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="input-group"><span class="input-group-text">Nama Tamu</span><input class="form-control" type="text" name="nama_tamu"></div>
                            <div class="input-group"><span class="input-group-text">Jumlah Hadir</span><input class="form-control" type="text" name="jumlah_tamu"></div>  
                            {{-- <div class="input-group"><span class="input-group-text">Nomor WA</span><input class="form-control" type="text" name="nomor_tamu" placeholder="+62" ></div> --}}
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Tambah Tamu</button></div>
                    </div>
                </div>
                </form>
            </div>
            <div class="modal fade" id="edit">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="/dashboardadmin/edittamu" method="post">
                            @csrf
                        <div class="modal-header">
                            <h4 class="modal-title">Edit Data Tamu</h4><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="input-group"><span class="input-group-text">ID Tamu</span><input class="form-control" type="text" name="id_tamu" id="id_tamu" readonly></div>
                            <div class="input-group"><span class="input-group-text">Nama Tamu</span><input class="form-control" type="text" name="nama_tamu" id="nama_tamu"></div>
                            <div class="input-group"><span class="input-group-text">Jumlah Hadir</span><input class="form-control" type="text" name="jumlah_tamu" id="jumlah_tamu"></div> 
                            {{-- <div class="input-group"><span class="input-group-text">Nomor WA</span><input class="form-control" type="text" name="nomor_tamu" id="nomor_tamu"></div>   --}}
                        </div>
                        <div class="modal-footer"><button class="btn btn-light" type="button" data-bs-dismiss="modal">Close</button><button class="btn btn-primary" type="submit">Submit</button></div>
                    </form>
                    </div>
                </div>
            </div>
            <footer class="bg-white sticky-footer">
                <div class="container my-auto">
                    <div class="text-center my-auto copyright"><span>Copyright © Memoire 2022</span></div>
                </div>
            </footer>
        </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a>
    </div>

    <script>
        $('#edit').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var nama = button.data('nama_tamu')
            var id = button.data('id_tamu')
            var jmlorang = button.data('jumlah_tamu')
            // var nmrtamu = button.data('nomor_tamu')

            var modal =$(this)
            modal.find('.modal-body #nama_tamu').val(nama);
            modal.find('.modal-body #jumlah_tamu').val(jmlorang);
            modal.find('.modal-body #id_tamu').val(id);
            // modal.find('.modal-body #nomor_tamu').val(nmrtamu);

        })
    </script>

    <script>
        // $("#btnCopy").addEventListener("click",function(){
        //     var x = document.getElementById("btnCopy").value;
        //     alert(x);
            
        // });
    </script>

    <script>
        $(document).ready(function () {
            $('#myTable').DataTable({
                // scrollX: true,
            });
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
@endsection