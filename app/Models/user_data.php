<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_data extends Model
{
    protected $table = 'user_data';

    protected $primaryKey = 'id_tamu';

    protected $keyType = 'integer';
    public $timestamps = false;
}
