@extends('admintemplate')

<head>
    <title>
        Scann QR
    </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.common.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
</head>
@section('Content')
<body class="bg-gradient-primary" style="background: url('{{asset('assets/img/covertamu.jpg')}}') center / cover;">
    <div class="container">
        <div class="d-flex d-sm-flex d-md-flex justify-content-center align-items-center justify-content-sm-center align-items-sm-center justify-content-md-center align-items-md-center" style="height: 550px;">
            <div class="row d-flex align-items-center" style="height: 550px;">
                <div class="col-md-9 col-lg-12 col-xl-10 d-lg-flex justify-content-lg-center align-items-lg-center" style="margin-top: 17px;">
                    <div class="card" style="width: 400px;background: rgba(255,255,255,0.07);">
                        <div class="card-body">
                            <h2 class="text-center card-title" style="color: rgb(0,0,0);">Scan Your Ticket</h2>
                            <div class="card" style="background: rgba(255,255,255,0.25);">
                                <div class="card-body">
                                    <form action="/memoire/absentamu" id="submitform" method="post">
                                        @csrf
                                        <input type="hidden" name="namatamu" id="isiqr">
                                    </form>
                                    <div id="reader" width="200px"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://unpkg.com/html5-qrcode" type="text/javascript"></script>
    <script>
        function onScanSuccess(decodedText, decodedResult) {
        // handle the scanned code as you like, for example:
            send_respons(decodedText);
            // console.log(`Code matched = ${decodedText}`, decodedResult);
        }

        function onScanFailure(error) {
        // handle scan failure, usually better to ignore and keep scanning.
        // for example:
            // console.warn(`Code scan error = ${error}`);
        }

        let html5QrcodeScanner = new Html5QrcodeScanner(
        "reader",
        { fps: 10, qrbox: {width: 250, height: 250} },
        /* verbose= */ false);
        html5QrcodeScanner.render(onScanSuccess, onScanFailure);

        function send_respons(decodedText){
            document.getElementById('isiqr').value= decodedText;
            $('#submitform').submit();
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
@endsection