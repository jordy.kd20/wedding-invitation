@extends('template')
<head>
    <title>Undangan Ardy & Cynthia</title>
</head>

@section('Content')
<body>
    <header style="height: 740px; background: url('{{asset('assets/img/cover.jpg')}}') center / cover;">
        <div class="container" style="padding-top: 228px;">
            <div class="card" style="background: rgba(255,255,255,0);border-color: rgba(33,37,41,0);">
                <div class="card-body" style="padding-top: 100px;">
                    <h3 class="d-flex d-xl-flex justify-content-center justify-content-xl-center" style="color: rgb(255,255,255);">Kepada Yth</h3>
                    <h4 class="d-flex justify-content-center" style="color: rgb(255,255,255);padding-top: 10px;padding-bottom: 10px;">{{$nama}}</h4>
                    <p class="d-flex justify-content-center align-items-center card-text" style="color: rgb(255,255,255);text-align: center;font-size: 17px;">Kami Mengunadang Bapak/Ibu/Saudara/i, Untuk Menghadiri Momen Spesail Kami</p>
                    <h1 class="d-flex justify-content-center" style="color: rgb(255,255,255);font-family: Aladin, serif;font-size: 38px;">ARDY &amp; CYNTHIA</h1>
                    <div class="d-flex justify-content-center"><a class="btn btn-primary d-flex justify-content-center" role="button" href="#open" onclick="playmusic()">❤ Buka Undangan&nbsp;</a></div>
                </div>
            </div>
        </div>
    </header>
    <section id="open" style="background: #E3E7EB;padding-bottom: 20PX;">
        <div class="container">
            <div style="color: rgb(0,0,0);">
                <div class="card" style="background: rgba(255,255,255,0);border-color: rgba(0,0,0,0);">
                    <div class="card-body" style="border-color: rgba(0,0,0,0);">
                        <h6 class="d-flex justify-content-center" style="font-family: Aladin, serif;font-size: 23px;">The Wedding of</h6>
                        <h1 class="d-flex justify-content-center" style="font-family: Aladin, serif;font-size: 38px;">ARDY &amp; CYNTHIA</h1>
                    </div>
                </div>
            </div>
            <div class="carousel slide" data-bs-ride="carousel" id="carousel-1">
                <div class="carousel-inner">
                    <div class="carousel-item active"><img class="w-100 d-block" src="{{url('assets/img/16.jpg')}}" alt="Slide Image"></div>
                    <div class="carousel-item"><img class="w-100 d-block" src="{{url('assets/img/15.jpg')}}" alt="Slide Image"></div>
                    <div class="carousel-item"><img class="w-100 d-block" src="{{url('assets/img/11.jpg')}}" alt="Slide Image"></div>
                    <div class="carousel-item"><img class="w-100 d-block" src="{{url('assets/img/5.jpg')}}" alt="Slide Image"></div>
                </div>
                <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-bs-slide="prev"><span class="carousel-control-prev-icon"></span><span class="visually-hidden">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-bs-slide="next"><span class="carousel-control-next-icon"></span><span class="visually-hidden">Next</span></a></div>
                <ol class="carousel-indicators">
                    <li data-bs-target="#carousel-1" data-bs-slide-to="0" class="active"></li>
                    <li data-bs-target="#carousel-1" data-bs-slide-to="1"></li>
                    <li data-bs-target="#carousel-1" data-bs-slide-to="2"></li>
                    <li data-bs-target="#carousel-1" data-bs-slide-to="3"></li>
                </ol>
            </div>
        </div>
    </section>
    <section style="background: #9DC0D2;">
        <div class="container" style="background: #9DC0D2;padding-top: 10PX;padding-bottom: 10px;">
            <div style="border-color: rgba(33,37,41,0);">
                <div class="card" style="background: rgba(255,255,255,0);">
                    <div class="card-body" style="border-color: rgba(33,37,41,0);">
                        <h6 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);">e-Ticket Pernikahan</h6>
                        <h2 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);padding-top: 18px;font-family: Aladin, serif;font-size: 28.44px;">ARDY &amp; CYNTHIA</h2>
                        <h6 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);padding-top: 18px;">Nama:</h6>
                        <h6 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);padding-top: 15px;padding-bottom: 15px;">{{$nama}}</h6>
                        {{-- <img class="d-flex justify-content-center card-title" src="data:image/png;base64,{{base64_encode(QrCode::format('png')->generate($nama))}}" alt=""> --}}
                        <div class="d-flex justify-content-center card-title">{{QrCode::size(200)->generate($nama)}}</div>
                        {{-- <img src="{!!$message->embedData(QrCode::format('png')->generate('Embed me into an e-mail!'), 'QrCode.png', 'image/png')!!}"> --}}
                        <h6 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);padding-top: 10px;">Keterangan:</h6>
                        <p class="card-text" style="font-size: 14px;color: rgb(255,255,255);">1. Setiap Tamu Undangan Wajib Hadir menunjukan&nbsp; undangan ini. 
                            {{-- <br> 2. 1 undangan hanya berlaku untuk {{$jmltamu}} orang.  --}}
                            <br> 2. Wajib mematuhi protokol kesehatan</p>
                    </div>
                    <div style="border-color: rgba(33,37,41,0);background: rgba(255,255,255,0);">
                        <div class="card" style="background: rgba(255,255,255,0.61);">
                            <div class="card-body">
                                <h4 class="d-flex d-md-flex justify-content-center justify-content-md-center card-title">Photo booth</h4>
                                <h6 class="text-muted card-subtitle mb-2"></h6>
                                <p class="card-text" style="text-align: center;">Dapat Ditukarkan dengan menunjukan tiket ini. Berikan Tiket ini pada petugas photobooth jangan menekan button sendiri!!</p>
                                @php
                                    $photobooth = DB::table('user_data')->where('id_tamu',$idtamu)->value('photobooth');
                                @endphp
                                @if ($photobooth==0)
                                <div class="d-flex justify-content-center"><a class="btn btn-primary" href="/photobooth/redemtiket/{{$idtamu}}">Tukar Tiket</a></div>
                                @else
                                <div class="d-flex justify-content-center"><button class="btn btn-warning" disabled>Tiket Sudah Ditukar</button></div>
                                @endif
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="background: #E3E7EB;">
        <div class="container" style="padding-top: 20px;padding-bottom: 20px;">
            <div>
                <div class="row">
                    <div class="col">
                        <div>
                            <div class="d-flex justify-content-center"><img class="d-flex justify-content-center pulse animated infinite" src={{url("assets/img/Untitled%20design%20(1).png")}} style="height: 200px;"></div>
                            <h4 class="d-flex justify-content-center" style="font-family: Allura, serif;font-size: 29px;">We Are</h4>
                            <h1 class="d-flex justify-content-center" style="font-family: Allura, serif;font-size: 29px;">Getting Married</h1>
                            <h6 class="d-flex justify-content-center" style="padding-top: 15px;padding-bottom: 15px;font-family: 'Source Sans Pro', sans-serif;">Salam Sejahterah</h6>
                            <p class="d-flex justify-content-center" style="text-align: center;font-size: 14px;">Tuhan Membuat segala sesuatu indah ppada waktunya, indah saat DIA mempertemukan, Indah saat DIA menumbuhkan kasih, dan Indah saat DIA Mempersatukan Kami Dalam Pernikahan Kudus.</p>
                            <div class="d-flex justify-content-center" style="padding-bottom: 20px;"><img data-aos="fade-left" data-aos-duration="900" src="{{url('assets/img/3.jpg')}}" style="width: 250px;box-shadow: 0px 0px;border-radius: 170px;"></div>
                            <h1 class="d-flex justify-content-center" style="font-family: Aladin, serif;">Ardy</h1>
                            <h5 class="d-flex justify-content-center">Ardy Wiendarto, ST</h5>
                            <p style="text-align: center;font-size: 14px;">Putra dari bapak Budy Wiendarto <br> &amp; Ibu Tjan Sri Wahyuliati</p>
                            <h5 class="d-flex justify-content-center" style="padding-top: 10px;padding-bottom: 10px;font-family: Allura, serif;font-size: 30px;">&amp;</h5>
                            <div class="d-flex justify-content-center" style="padding-bottom: 20px;"><img data-aos="fade-right" data-aos-duration="900" src="{{url('assets/img/4.jpg')}}" style="width: 250px;box-shadow: 0px 0px;border-radius: 170px;"></div>
                            <h1 class="d-flex justify-content-center" style="font-family: Aladin, serif;">Cynthia</h1>
                            <h5 class="d-flex justify-content-center">apt. Cynthia Kusuma Dewi S.Farm</h5>
                            <p style="text-align: center;font-size: 14px;">Putra dari Bapak Putu Khuta Parthadinata (siong-siong) <br> &amp; Ibu Tuti Soesetiawati</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="background: #9DC0D2;">
        <div class="container" style="padding-top: 30px;padding-bottom: 30px;">
            <div class="d-flex justify-content-center"><img class="pulse animated infinite" src="{{url('assets/img/Untitled%20design%20(1).png')}}" style="height: 200px;"></div>
            <div>
                <h1 class="d-flex justify-content-center" style="color: rgb(255,255,255);font-family: Allura, serif;">Save The Date</h1>
                <p style="text-align: center;font-size: 12px;color: rgb(255,255,255);">Dengan segala Kerendahan hati dan ungkapan Syukur atas karunia Tuhan, Kami Mengundang Bapak/Ibu/Saudara/i untuk menghadiri acara pernikahan kami yang diselenggarakan pada:</p>
                <div class="row">
                    <div class="col">
                        <div class="card" data-aos="fade-up" data-aos-duration="900" style="background: rgba(255,255,255,0.74);">
                            <div class="card-body">
                                <h4 class="d-flex justify-content-end card-title">Resepsi&nbsp;</h4>
                                <h4 class="d-flex justify-content-end card-title">Pernikahan</h4>
                                <br>
                                <h5 class="d-flex justify-content-center card-title">Ruangan Kerinci Rinjani</h5> 
                                <h6 class="text-muted card-subtitle mb-2"></h6>
                                <p class="card-text" style="text-align: right;">Sabtu, 15 Oktober 2022 18.00 Graha Mahameru Surabaya</p>
                                <div class="d-flex justify-content-end"><a class="btn btn-primary" role="button" style="border-radius: 50px;" href="https://goo.gl/maps/Xf3XbGjYWKG1nEQo6">Lihat Lokasi ▶&nbsp;</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="background: #E3E7EB;">
        <div class="container" style="padding-top: 20px;padding-bottom: 20px;">
            <h1 class="text-center">Gallery</h1>
            <div data-aos="fade-up-right" data-aos-duration="1000" style="text-align: center;"><img src="{{url('assets/img/5.jpg')}}" style="height: 100px;padding-right: 10px;padding-top: 5px;"><img src="{{url('assets/img/11.jpg')}}" style="height: 100px;padding-top: 5px;padding-right: 10px;"><img src="{{url('assets/img/9.jpg')}}" style="height: 100px;padding-top: 5px;padding-right: 10px;"><img src="{{url('assets/img/8.jpg')}}" style="height: 100px;padding-top: 5px;padding-right: 10px;"><img src="{{url('assets/img/15.jpg')}}" style="height: 100px;padding-top: 5px;padding-right: 10px;">
                <picture><img src="{{url('assets/img/16.jpg')}}" style="height: 100px;padding-top: 5px;padding-right: 10px;"></picture>
            </div>
        </div>
    </section>
    <section style="background: #9DC0D2;">
        <div class="container">
            <div class="d-flex justify-content-center" style="padding-top: 30px;padding-bottom: 30px;">
                <div class="card" style="border-color: rgba(33,37,41,0);background: rgba(255,255,255,0.52);">
                    <div class="card-body" style="border-color: rgba(33,37,41,0);">
                        <h4 class="d-flex justify-content-center card-title">Protokol Kesehatan</h4>
                        <div class="d-flex justify-content-center"><img src="{{url('assets/img/masker.png')}}" style="height: 100px;"></div>
                        <p class="d-flex justify-content-center" style="font-size: 12px;text-align: center;">Selalu memakai masker selama acara berlangsung</p>
                        <div class="d-flex justify-content-center"><img src="{{url('assets/img/cucitangan.png')}}" style="height: 100px;"></div>
                        <p class="d-flex justify-content-center" style="font-size: 12px;text-align: center;">Mencuci tangan sebelum dan sesudah memasuki lokasi acara</p>
                        <div class="d-flex justify-content-center"><img src="{{url('assets/img/shakehand.png')}}" style="height: 100px;"></div>
                        <p class="d-flex justify-content-center" style="font-size: 12px;">Dilarang berjabat tagan</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="background: url('{{asset('assets/img/6.jpg')}}') center / cover space;height: 760px;">
        <div class="container">
            <div style="padding-top: 460px;">
                <div class="card" style="border-color: rgba(33,37,41,0);background: rgba(255,255,255,0);">
                    <div class="card-body" style="border-color: rgba(33,37,41,0);">
                        <h1 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);font-size: 30px;padding-bottom: 20px;font-family: Allura, serif;">Terimah Kasih</h1>
                        <h5 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);font-family: Acme, sans-serif;font-size: 14px;padding-bottom: 10px;">Kami Yang Berbahagia</h5>
                        <h5 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);text-align: center;font-size: 15px;padding-bottom: 10px;font-family: Aclonica, sans-serif;">Kedua Mempelai &amp; Keluarga Besar</h5>
                        <h3 class="d-flex justify-content-center card-title" style="color: rgb(255,255,255);font-family: Acme, sans-serif;">#ARDYWInsCYNTHIAslove</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container" style="padding-top: 30px;padding-bottom: 30px;">
            <div class="d-flex justify-content-center"><img src="{{url('assets/img/WhatsApp%20Image%202022-09-08%20at%2021.54.53.jpg')}}" style="height: 100px;"></div>
        </div>
    </section>
    <script>
        function playmusic(){
            var music = new Audio('/assets/music/istilloveyou.mp3');
            music.play();
        }
    </script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{url('assets/js/bs-init.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
    <script src="{{url('assets/js/stylish-portfolio.js')}}"></script>
</body>
@endsection