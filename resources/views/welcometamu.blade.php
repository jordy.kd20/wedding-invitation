<head>
    <title>
        Welcome Tamu
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Undangan Ardy &amp; cynthia</title>
    <link rel="stylesheet" href="{{url('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aclonica&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Acme&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aguafina+Script&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aladin&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allura&amp;display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Arizonia&amp;display=swap">
    <link rel="stylesheet" href="{{url('assets/fonts/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/fonts/simple-line-icons.min.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css">
</head>
<body style="background: url('{{asset('assets/img/covertamu.jpg')}}') center / cover;">
    <div class="container">
        <div class="row justify-content-center" style="margin-right: 0px;margin-left: 0px;">
            <div class="col-md-9 col-lg-12 col-xl-10" style="margin-top: 0px;border-color: rgba(133,135,150,0);">
                <div class="card shadow-lg o-hidden border-0 my-5" style="border-color: rgba(133,135,150,0);background: rgba(255,255,255,0.1);">
                    <div class="card-body p-0" style="border-color: rgba(133,135,150,0);background: rgba(255,255,255,0.2);">
                        <div class="row" style="height: 400px;">
                            <div class="col d-flex d-sm-flex d-xxl-flex justify-content-center align-items-center justify-content-sm-center align-items-sm-center justify-content-xxl-center align-items-xxl-center">
                                <div class="card d-xxl-flex" style="background: rgba(255,255,255,0);border-color: rgba(255,255,255,0);">
                                    <div class="card-body" style="padding-bottom: 16px;padding-top: 16px;border-color: rgba(133,135,150,0);">
                                        <div style="height: 200px;color: rgb(0,0,0);">
                                            <div class="card" style="background: rgba(255,255,255,0);border-color: rgba(0,0,0,0);">
                                                <div class="card-body" style="border-color: rgba(133,135,150,0);">
                                                    <h1 class="d-flex justify-content-center" style="font-size: 60px;">Welcome,</h1>
                                                </div>
                                            </div>
                                            <div class="card" style="background: rgba(255,255,255,0);border-color: rgba(255,255,255,0);">
                                                <div class="card-body">
                                                    <h2 class="d-sm-flex justify-content-sm-center card-title">{{$namatamu}}</h2>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-flex d-lg-flex justify-content-center align-items-center justify-content-lg-center"><a class="btn btn-primary" href="/scan">Scan Undangan</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/bs-init.js?h=e2b0d57f2c4a9b0d13919304f87f79ae"></script>
    <script src="{{url('asset/js/bs-init.js')}}"></script>
    <script src="{{url('asset/js/theme.js')}}"></script>
    <script src="{{url('assets/bootstrap/js/bootstrap.min.css')}}"></script>
    
  
    <script src="assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/js/theme.js?h=79f403485707cf2617c5bc5a2d386bb0"></script>
</body>
