<?php

namespace App\Http\Controllers;

use App\Models\user_data;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Auth;
use Firebase\Auth\Token\Exception\InvalidToken;
use GPBMetadata\Google\Firestore\Admin\V1\Index;
use Illuminate\Support\Arr;
use Kreait\Firebase\Exception\Auth\RevokedIdToken;
use PhpParser\Node\Stmt\Return_;
use Symfony\Component\VarDumper\VarDumper;
use RealRashid\SweetAlert\Facades\Alert;
use PDF;


class backendcontroler extends Controller
{ 
  public function logout(){
    Session::remove("admin");
    return redirect('/dashboard');
  }

  public function loginawal(){
    if(Session::has('admin')){
      return redirect('/dashboardadmin');
    }else{
      return view('login');
    }
    
  }

  public function login(Request $req){
  
  if((Session::has('admin'))){
    return redirect('/dashboardadmin');
  }
  else{
    if(isset($req->username)){
      if($req->username == "admin"){
          if(isset($req->password)){
            
              if($req->password == "011095Cc"){
                
                Session::put("admin","ada");
                return redirect('/dashboardadmin');
                
              } 
          }
          else{    
            
            Alert::error('error', 'passwword Salah');
            return redirect('/dashboard');
          }
      }
      else{
       
        Alert::error('error', 'Username Salah');
        return redirect('/dashboard');
      }
      
    }
    else{
        Alert::error('error', 'Username Salah');
        return redirect('/dashboard');
    }
  }

  
       
  }

  public function dashboard(){
    if(Session::get("admin")=="ada"){
      $datall['datatamu'] = user_data::get();
      return view('dashboard',['isi'=>1])->with($datall);
    }else{
      return redirect('/dashboard');
    }

    
  }

  public function tambahtamu(Request $req){
    $namatamu = $req->nama_tamu;
    $jmlorang = $req->jumlah_tamu;

    $ceknama = user_data::where('nama_tamu',$namatamu)->count();

    if($ceknama==0){
      $tambahtamu = new user_data;
      $tambahtamu->nama_tamu = $namatamu;
      $tambahtamu->jumlah_tamu = $jmlorang;
      $tambahtamu->save();
      Alert::success('Success', 'Berhasil Menambahkan Data');
      return redirect('/dashboardadmin');
    }
    else{
      Alert::error('error', 'Nama Tamu Sudah Ada');
      return redirect('/dashboardadmin');
    }
  }


  public function edittamu(Request $req){
    
    $id = $req->id_tamu;
    $nama = $req->nama_tamu;
    $jmltamu = $req->jumlah_tamu;
    user_data::where('id_tamu',$id)->update([
      'nama_tamu' => $nama,
      'jumlah_tamu' => $jmltamu
    ]);
    Alert::success('Success', 'Berhasil Update Data');
    return redirect('/dashboardadmin');

  }

  public function deletetamu($id){
    user_data::where('id_tamu',$id)->delete();
    Alert::success('Success', 'Berhasil Delete Data');
    return redirect('/dashboardadmin');
  }

  public function tampilanudangan($id){

    $nama = user_data::where('id_tamu',$id)->value('nama_tamu');
    $jumlah_tamu = user_data::where('id_tamu',$id)->value('jumlah_tamu');

    return view('undangan',["nama"=>$nama,"jmltamu"=>$jumlah_tamu,"idtamu"=>$id]);
  }
  public function redemtiket($id){
      user_data::where('id_tamu',$id)->update(['photobooth'=>1]);
      return redirect('/memoire/undangan/'.$id.'#open');
  }

  public function absentamu(Request $req){
    $countuser = user_data::where('nama_tamu',$req->namatamu)->count();
    if($countuser!=0){
      user_data::where('nama_tamu',$req->namatamu)->update(['kehadiran'=>1]);
      return view('welcometamu',['namatamu'=>$req->namatamu]);
    }
    else{
      Alert::error('error', 'nama tidak ditemukan');
      return redirect('/scan');
    }
    
  }

  public function resetkehadiran(){
    $data = user_data::get();
    foreach($data as $item){
      user_data::where('id_tamu',$item->id_tamu)->update(['kehadiran'=>0]);
    }
    return redirect('/logout');
  }

  public function exportpdf(){
    
    $data = user_data::all();
    view()->share('data',$data);
    $pdf = PDF::loadview('exportpdf')->setPaper('a4', 'landscape');
    return $pdf->download('data-tamu.pdf');

  }

  public function absentamumanual($id){
    $nama = user_data::where('id_tamu',$id)->value('nama_tamu');
    user_data::where('id_tamu',$id)->update(['kehadiran'=>1]);
    Alert::success('Success', 'Berhasil Absen '.$nama);
    return redirect('/dashboardadmin');
  }

  public function cekdb(){
    // $students = app('firebase.firestore')->database()->collection('user_data')->documents('d3d065c42d96450d8375');  
    // dd($students);
    // dd('Total records: '.$students->size());  
    
//     foreach($students as $stu) {  
//         if($stu->exists()){  
     
//         dd('Student Name = '.$stu->id());  
//    }  
//  }
    
  }
}
