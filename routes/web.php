<?php

use App\Http\Controllers\backendcontroler;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/cekdb',[\App\Http\Controllers\backendcontroler::class, 'cekdb']);
Route::view('/','login');
Route::get('/resetkehadiran',[backendcontroler::class, 'resetkehadiran']);
Route::get('/exportpdf', [backendcontroler::class, 'exportpdf']);

Route::get('/dashboard', [backendcontroler::class, 'loginawal']);
Route::get('/logout', [\App\Http\Controllers\backendcontroler::class, 'logout']);
Route::view('/memoire/undangan','undangan');
Route::get('/memoire/undangan/{id}',[\App\Http\Controllers\backendcontroler::class, 'tampilanudangan']);
Route::get('/dashboardadmin', [\App\Http\Controllers\backendcontroler::class, 'dashboard']);
Route::get('/dashboardadmin/delete/{id}',[\App\Http\Controllers\backendcontroler::class, 'deletetamu']);

Route::post('/dashboard/login',[\App\Http\Controllers\backendcontroler::class,'login']);
Route::post('/dashboard/tambahorang', [\App\Http\Controllers\backendcontroler::class, 'tambahtamu']);
Route::post('/dashboardadmin/edittamu',[\App\Http\Controllers\backendcontroler::class, 'edittamu']);
Route::get('/photobooth/redemtiket/{id}',[backendcontroler::class, 'redemtiket']);
Route::post('/memoire/absentamu',[backendcontroler::class, 'absentamu']);
Route::get('/absentamu/{id}',[backendcontroler::class, 'absentamumanual']);

Route::view('/scan','scaner');

Route::get('/insert',function(){
    $sturef = app('firebase.firestore')->database()->collection('user_data')->newDocument();
    $sturef->set([
        'nama_tamu' => 'Ignatius Steven'
    ]);

});